Source files of **Morevna Episode 3.**

* [Homepage](https://morevnaproject.org/anime/episode-3/)
* [Build instructions](https://morevnaproject.org/2016/12/12/build-sources-morevna-episode-3/)

You can fetch contents of this directory using RSync:

```
rsync -avz --exclude render rsync://archive.morevnaproject.org/sources/morevna-ep3/ ~/morevna-ep3/
```

Alternatively, you can [clone from GitLab](https://gitlab.com/OpenSourceAnimation/morevna-ep3). 

Note:  before cloning from GitLab, please make sure that you have [Git LFS extension](https://git-lfs.github.com/) installed.